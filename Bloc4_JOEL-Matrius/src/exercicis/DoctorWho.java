package exercicis;

import java.util.Scanner;

public class DoctorWho {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int filaBomba = src.nextInt();
		int columnaBomba = src.nextInt();
		
		int filaObjetivo = src.nextInt();
		int columnaObjetivo= src.nextInt();
		
		int [][] matriu = new int [10] [10];
		
		matriu[filaBomba][columnaBomba]= 1;
		boolean muerte=false;
		
		//pa derecha
		if (columnaBomba+1>10) {
			muerte=false;
		} else {matriu[filaBomba][columnaBomba+1]= 2;}
		
		if (columnaBomba+2>10) {
			muerte=false;
		} else {matriu[filaBomba][columnaBomba+2]= 2;}
		
		//pa izquierda
		if (columnaBomba-1<0) {
			muerte=false;
		} else {matriu[filaBomba][columnaBomba-1]= 2;}
		
		if (columnaBomba-2<0) {
			muerte=false;
		} else {matriu[filaBomba][columnaBomba-2]= 2;}
		
		//pa arriba
		
		if (filaBomba-1<0) {
			muerte=false;
		} else {matriu[filaBomba-1][columnaBomba]= 2;}
		
		if (filaBomba-2<0) {
			muerte=false;
		} else {matriu[filaBomba-2][columnaBomba]= 2;}
		
		//pa abajo 
		if (filaBomba+1<10) {
			muerte=false;
		} else {matriu[filaBomba+1][columnaBomba]= 2;}
		
		if (filaBomba+2<10) {
			muerte=false;
		} else {matriu[filaBomba+2][columnaBomba]= 2;}
				
		//diagonal derecha arriba 
		if (filaBomba-1<0) {
			muerte=false;
		} else {matriu[filaBomba-1][columnaBomba+1]= 2;}
		
		//diagonal izquierda arriba 
		if (filaBomba-1<0 || columnaBomba-1<=0) {
			muerte=false;
		} else {matriu[filaBomba-1][columnaBomba-1]= 2;}
		
		//diagonal derecha abajo
		if (filaBomba+1<0 || columnaBomba+1<=0) {
			muerte=false;
		} else {matriu[filaBomba+1][columnaBomba+1]= 2;}
		
		//diagonal izquierda abajo 
		if (filaBomba+1<0 || columnaBomba-1<=0) {
			muerte=false;
		} else {matriu[filaBomba+1][columnaBomba-1]= 2;}
		
		
		
		if (matriu[filaObjetivo][columnaObjetivo]==2) {
			muerte=true;
		}
		
		System.out.println(muerte);
	}

}
