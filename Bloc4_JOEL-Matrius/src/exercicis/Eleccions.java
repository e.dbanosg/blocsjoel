package exercicis;

import java.util.Scanner;

public class Eleccions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);

		int tamany = src.nextInt();
		char [][] matriu= new char [tamany] [tamany];
		
		for (int i =0; i <tamany; i++) {
			for (int j=0;j<tamany;j++) {
				matriu[i][0]='X';
				matriu[i][tamany-1]='X';
				matriu[0][j]='X';
				matriu[tamany-1][j]='X';
				if (matriu[i][j]=='X'){
					matriu[i][j]='X';
				} else if (j==i) {
					matriu[i][j]='X';
				} else if (j+i==tamany-1) {
					matriu[i][j]='X';
				} else {
					matriu[i][j]='.';
				}
			}
		}
		
		for (int i =0; i<tamany; i++) {
			for (int j =0; j<tamany; j++) {
				System.out.print(matriu[i][j] + " ");
			}
			System.out.println();
		}
		
		
	}

}
