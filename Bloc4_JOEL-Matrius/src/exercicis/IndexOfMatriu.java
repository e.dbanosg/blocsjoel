package exercicis;

import java.util.Scanner;

public class IndexOfMatriu {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);

		int Files = src.nextInt();
		int Columnes = src.nextInt();
		
		int [][] matriu= new int [Files] [Columnes];
		
		for (int i =0; i<Files; i++) {
			for (int j =0; j<Columnes; j++) {
				matriu[i][j]=src.nextInt();
			}
		}
		
		
		int K = src.nextInt();
		boolean esta=false;
		int posfila=0;
		int poscolumna=0;
		
		for (int i =0; i<Files; i++) {
			for (int j =0; j<Columnes; j++) {
				if (matriu[i][j]==K) {
					esta=true;
					posfila=i;
					poscolumna=j;
					} 
				}
			}
		
		
		if (esta) {
			System.out.println(posfila + " " + poscolumna);
		} else {
			System.out.println(-1 + " " + -1);
		}
		
	}

}
