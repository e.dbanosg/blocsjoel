package exercicis;

import java.util.Scanner;

public class MaximDeMatriu {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int casos=src.nextInt();
		
		
		for(;casos>0;casos--) {
			int Files = src.nextInt();
			int Columnes = src.nextInt();
			
			int [][] matriu= new int [Files] [Columnes];
			int posi=-1;
			int posj=-1;
			for (int i =0; i<Files; i++) {
				for (int j =0; j<Columnes; j++) {
					matriu[i][j]=src.nextInt();
				}
			}
			
			int max=Integer.MIN_VALUE;
			
			for (int i =0; i<Files; i++) {
				for (int j =0; j<Columnes; j++) {
					if (matriu[i][j]>max) {
						max=matriu[i][j];
						posi=i+1;
						posj=j+1;
					}
				}
			}	
			System.out.println(posi + " " + posj);
		}
		
		
	}

}
