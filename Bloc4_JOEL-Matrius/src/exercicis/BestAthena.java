package exercicis;

import java.util.Scanner;

public class BestAthena {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt();
		
		for (; casos > 0; casos --) { 
			int Files = src.nextInt();
			int Columnes = src.nextInt();
			
			int [][] matriu= new int [Files] [Columnes];
			
			for (int i =0; i<Files; i++) {
				for (int j =0; j<Columnes; j++) {
					matriu[i][j]=src.nextInt();
				}
			}
			
			int X = src.nextInt();
			int Y = src.nextInt();
			
			int cont=0;
			
			if (matriu[X][Y]==1) {
				if (X+1<Files) {
					if (matriu[X+1][Y]==2) {
						cont++;
					}
				}
				if (X-1<Files) {
					if (matriu[X-1][Y]==2) {
						cont++;
					}
				}
				if (Y+1<Columnes) {
					if (matriu[X][Y+1]==2) {
						cont++;
					}
				}
				if (Y-1<Columnes) {
					if (matriu[X][Y-1]==2) {
						cont++;
					}
				}
				if (X+1<Files && Y+1<Columnes) {
					if (matriu[X+1][Y+1]==2) {
						cont++;
					}
				}
				if (X-1<Files && Y-1<Columnes) {
					if (matriu[X-1][Y-1]==2) {
						cont++;
					}
				}
				if (X-1<Files && Y+1<Columnes) {
					if (matriu[X-1][Y+1]==2) {
						cont++;
					}
				}
				if (X+1<Files && Y-1<Columnes) {
					if (matriu[X+1][Y-1]==2) {
						cont++;
					}
				}
				System.out.println(cont);
			} else {System.out.println(-1);}
			
			
		}
		
		
		
		
	}

}
