package exercicis;

import java.util.Scanner;

public class SumaFilesIColumnes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);

		int Files = src.nextInt();
		int Columnes = src.nextInt();
		
		int [][] matriu= new int [Files] [Columnes];
		
		for (int i =0; i<Files; i++) {
			for (int j =0; j<Columnes; j++) {
				matriu[i][j]=src.nextInt();
			}
		}
		
		int K = src.nextInt();
		int sumafila=0;
		int sumacolumna=0;
		
		for (int i =0; i <Columnes; i++) {
			sumafila+= matriu[K][i];
		}
		
		for (int i =0; i <Files; i++) {
			sumacolumna+=  matriu[i][K];
		}
		
		System.out.print(sumafila + " " + sumacolumna);
		
	}

}
