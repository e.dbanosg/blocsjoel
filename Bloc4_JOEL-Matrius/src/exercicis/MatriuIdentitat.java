package exercicis;

import java.util.Scanner;

public class MatriuIdentitat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);

		int tamany = src.nextInt();
		int [][] matriu= new int [tamany] [tamany];
		
		for (int i =0; i <tamany; i++) {
			for (int j=0;j<tamany;j++) {
				if (i==j) {
					matriu[i][j]=1;
				}
			}
		}
		
		for (int i =0; i<tamany; i++) {
			for (int j =0; j<tamany; j++) {
				System.out.print(matriu[i][j] + " ");
			}
			System.out.println();
		}
	}

}
