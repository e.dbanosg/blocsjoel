package exercicis;

import java.util.Scanner;

public class ComptarVocals {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		int casos=src.nextInt();
		src.nextLine();
		
		while (casos>0) {
			casos--;
			String frase= src.nextLine();
			int contador=0;
			int A=0;
			int E=0;
			int I=0;
			int O=0;
			int U=0;
			int longi= frase.length();
			
			while (contador<longi) {
				char c = Character.toLowerCase(frase.charAt(contador));
				if (c == 'a') {
					A += 1;
				} else if (c == 'e') {
					E += 1;
				} else if (c == 'i') {
					I += 1;
				} else if (c == 'o') {
					O += 1;
				} else if (c == 'u') {
					U += 1;
				}
				contador++;
			}
			System.out.println("A: " + A + " E: " + E + " I: " + I + " O: " + O + " U: " + U);
		}
	}

}
