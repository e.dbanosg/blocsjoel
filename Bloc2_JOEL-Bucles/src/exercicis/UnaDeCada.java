package exercicis;

import java.util.Scanner;

public class UnaDeCada {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		int casos= src.nextInt();
		
		src.nextLine();
		boolean majus= false;
		
		while (casos>0) {
			String frase = src.nextLine();
			String solucio = "";
			int i = 0;
			
			while (i<frase.length()) {
				if ((frase.charAt(i) >= 'A' && frase.charAt(i) <= 'Z') || (frase.charAt(i) >= 'a' && frase.charAt(i) <= 'z'))  {
					
					if (majus==true) {
						solucio= solucio + Character.toUpperCase(frase.charAt(i));
						
					} else {
						solucio= solucio + Character.toLowerCase(frase.charAt(i));
						
					}
			}
				else {
					solucio= solucio + frase.charAt(i);
					}
				majus = !majus;
			i++;
		}
			System.out.println(solucio);
			casos--;
		
		
	}

	}

}
