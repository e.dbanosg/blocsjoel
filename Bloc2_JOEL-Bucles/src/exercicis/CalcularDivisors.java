package exercicis;

import java.util.Scanner;

public class CalcularDivisors {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
        int casos = src.nextInt();
         
        while (casos>0) {
        	int n= src.nextInt();
        	int divisor=1;
        	String resultat= "";
        	
        	while (divisor<=n) {
        		if (n % divisor == 0) {
        			resultat= resultat + divisor + " ";
        		}
        		divisor++;
        	}
        	System.out.println(resultat);
        	casos--;
        }
        
	}

}
