package exercicis;

import java.util.Scanner;

public class factorial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src= new Scanner (System.in);
		int casos= src.nextInt();
		
		src.nextLine();
		
		while (casos>0) {
			int num=src.nextInt();
			long fact=1;
			while (num>0) {
				fact = fact * num;
				num--;
			}
			System.out.println(fact);
			casos--;
		}

	}

}

