package exercicis;

import java.util.Scanner;

public class Murcielago {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		String frase=src.nextLine();
		
		int longi= frase.length();
		int contador=0;
		int A=0;
		int E=0;
		int I=0;
		int O=0;
		int U=0;
		while (contador<longi) {
			char c = frase.charAt(contador);
			if (c == 'a') {
				A += 1;
			} else if (c == 'e') {
				E += 1;
			} else if (c == 'i') {
				I += 1;
			} else if (c == 'o') {
				O += 1;
			} else if (c == 'u') {
				U += 1;
			}
			contador++;
		}
	
		if (A==0||E==0||I==0||O==0||U==0) {
			System.out.println("FALTEN");
		} else {
			System.out.println("TOTES");
		}
	}

}
