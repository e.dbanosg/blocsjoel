package exercicis;

import java.util.Scanner;

public class ContarLletresFases {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src= new Scanner (System.in);
		String frase="";
				
		while (!frase.equals("FI")) {
			frase= src.nextLine();
			if (!frase.equals("FI")) {
				String ingles = frase.replaceAll("[^a-zA-Z]", ""); //elimina todos los caracteres que no son letras del alfabeto inglés de la cadena frase
				int longi= ingles.length(); //calcular longitud de la frase con solo caracteres
				System.out.println(longi + " ");
			}
		}
	}

}
