package exercicis;

import java.util.Scanner;

public class doblebucle {
	
	    public static void main(String[] args) {
	        Scanner src = new Scanner(System.in);
	        
	        int n = src.nextInt();
	        
	        String resultado = "";
	        int i = 0;

	        while (i < n+1) {
	            int j = 0;
	            while (j < i) {
	                resultado += i;
	                j++;
	            }
	            i++;
	        }

	        System.out.println(resultado);
	    }
	

}
