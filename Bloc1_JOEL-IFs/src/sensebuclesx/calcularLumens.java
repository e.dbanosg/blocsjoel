package sensebuclesx;

import java.util.Scanner;

public class calcularLumens {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		String x = src.nextLine();
		
		String[] parts = x.split(",");
		String part1 = parts[0];
		int part2 = Integer.parseInt(parts[1]);
		
		if ((part2 <= 0) && !(part1.equals("salo") || part1.equals("menjador") || part1.equals("dormitori") || part1.equals("cuina") || part1.equals("lavabo") || part1.equals("passadis"))) {
			System.out.println("ERROR: Habitacio i mides no reconegudes");
		} else if (part2 <= 0) {
			System.out.println("ERROR: Mida incorrecta");
		} else if (!(part1.equals("salo") || part1.equals("menjador") || part1.equals("dormitori") || part1.equals("cuina") || part1.equals("lavabo") || part1.equals("passadis"))) {
			System.out.println("ERROR: Habitacio no reconeguda");
		} else { 
		if (part1.equals("salo")) {
			int min= part2 * 100;
			int max= part2 * 250;
			System.out.println("La quantitat de lumens per el salo es de " + min + " a " + max + " lumens");
		} else if (part1.equals("menjador")) {
			int min= part2 * 350;
			int max= part2 * 500;
			System.out.println("La quantitat de lumens per el menjador es de " + min + " a " + max + " lumens");
		} else if (part1.equals("dormitori")) {
			int min= part2 * 50;
			int max= part2 * 150;
			System.out.println("La quantitat de lumens per el dormitori es de " + min + " a " + max + " lumens");
		} else if (part1.equals("cuina")) {
			int min= part2 * 200;
			int max= part2 * 300;
			System.out.println("La quantitat de lumens per la cuina es de " + min + " a " + max + " lumens");
		} else if (part1.equals("lavabo")) {
			int min= part2 * 150;
			int max= part2 * 200;
			System.out.println("La quantitat de lumens per el lavabo es de " + min + " a " + max + " lumens");
		} else if (part1.equals("passadis")) {
			int min= part2 * 100;
			int max= part2 * 200;
			System.out.println("La quantitat de lumens per el passadis es de " + min + " a " + max + " lumens");
		}
		}
		
	}

}
