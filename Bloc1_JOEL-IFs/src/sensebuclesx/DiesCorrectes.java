package sensebuclesx;

import java.util.Scanner;

public class DiesCorrectes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		int num = src.nextInt();
		if (num==366) {
			System.out.println("Correcte per un any bixest!");
		} 
		else if (num>=1 && num<=365) {
			System.out.println("Correcte per un any no bixest!");	
		}
		else {
			System.out.println("Incorrecte!");
		}
	}

}
