package sensebuclesx;

import java.util.Scanner;

public class posantordrealsllibres {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//N = int(input()) 	Permet llegir de teclat un nombre enter i ho guarda dins d’N
		//res = K - (N * M) 	Guarda a la variable res el resultat de l’operació
		//print(res)
		Scanner src = new Scanner(System.in);
		int N = src.nextInt();
		int M = src.nextInt();
		int K = src.nextInt();
		int res = K - (N * M);
		
		if (res<0) {
			System.out.println(0);
		} else {
			System.out.println(res);
		}
	}

}
