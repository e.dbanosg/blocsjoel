package sensebuclesx;

import java.util.Scanner;

public class Llibres {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		int N=src.nextInt();
		int M=src.nextInt();
		int K=src.nextInt();
		int mult;
		int div;
		
		if (N>=0 && M>=0 && K>=0) {
			mult=N*M;
			if (K<mult) {
				System.out.println(0);
			}	else if (mult>K) {
				div= mult-K;
				System.out.println(div);
			}	else if (K>mult) {
				div= K-mult;
				System.out.println(div);
			}	else {
				System.out.println(K-mult);
			}
		}
	
	}

}
