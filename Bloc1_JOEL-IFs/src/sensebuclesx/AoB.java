package sensebuclesx;

import java.util.Scanner;

public class AoB {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		int A = src.nextInt();
		int B = src.nextInt();
		
		if (A==B) {
			System.out.println(". . . . .");
			System.out.println(". . . . .");
			System.out.println(". . . . .");
			System.out.println(". . . . .");
			System.out.println(". . . . .");
		} else if (A>B) {
			System.out.println(". X X X .");
			System.out.println("X . . . X");
			System.out.println("X X X X X");
			System.out.println("X . . . X");
			System.out.println("X . . . X");
		} else {
			System.out.println("X X X X .");
			System.out.println("X . . . X");
			System.out.println("X X X X .");
			System.out.println("X . . . X");
			System.out.println("X X X X .");
		}
		
	}

}
