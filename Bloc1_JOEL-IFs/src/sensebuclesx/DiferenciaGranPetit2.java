package sensebuclesx;

import java.util.Scanner;

public class DiferenciaGranPetit2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner (System.in);		
		int n1 = src.nextInt();
		int n2 = src.nextInt();
		int n3 = src.nextInt();
	
		if (n1 >= n2 && n2 >= n3)
			System.out.println(n1 - n3);
		else if (n2 >= n1 && n1 >= n3)
			System.out.println(n2 - n3);
		else if (n2 >= n3 && n3 >= n1)
			System.out.println(n2 - n1);
		else if (n1 >= n3 && n3 >= n2)
			System.out.println(n1 - n2);
		else if (n3 >= n1 && n1 >= n2)
			System.out.println(n3 - n2);
		else
			System.out.println(n3 - n1);

	}

}
