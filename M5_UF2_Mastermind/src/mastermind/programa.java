package mastermind;

import java.util.Scanner;

/**
 * Classe que conte el menu i el distribuidor de tasques 
 * @author David Banos Garcia
 * @version 30-01-2024
 */
public class programa {
static Scanner src = new Scanner(System.in);
	
	/**
	 * Programa principal que fa de distribuidor de tasques
	 * @param args, no rep res util per distribuïr les tasques
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int op = 0;  //guarda l'opció escollida per l'usuari
		boolean nums=false;
		int []vector=new int[4];
		int num = 8;
		int numGuany=0;
		int numPerd=0;
		int numIn=0;
		
		do {
			op = mostrarMenú();
			
			switch (op) {
				case 1: vector= joc.generarNums();
						nums=true;
						System.out.println("Numeros generats correctament");
						System.out.println();
						break;
				case 2: if (nums) {
							num=joc.jugarPartida(vector);
						} else {
							System.out.println("No pots jugar fins que no estiguin generats els numeros");
						}
						break;
				case 3: if (num==0) {
							numPerd++;
						} else if (num==1) {
							numGuany++;
						} else if (num==2) {
							numIn++;
						}
						System.out.println("Numero partides guanyades= " + numGuany);
						System.out.println("Numero partides perdudes= "+ numPerd);
						System.out.println("Numero partides inacabades= " + numIn);
						break;
				case 0: System.out.println("Fi de joc");
						break;
				default: System.out.println("Opció incorrecte");
			}
			
		} while (op != 0);
		
	}
	
	/**
	 * Aquest metode s'encarrega de mostrar per pantalla el menu del joc,
	 * demanar l'opcio al jugador i comprovar que l'opcio es correcta. 
	 * @return retorna l'opcio escollida pel jugador
	 */
	private static int mostrarMenú() {
		// TODO Auto-generated method stub
		int op=0;
		boolean correcte=true;
		System.out.println("_______________________");
		System.out.println("Mastermind");
		System.out.println("_______________________");
		System.out.println("1.- Generar combinació");
		System.out.println("2.- Jugar");
		System.out.println("3.- Resultats acumulats");
		System.out.println("0.- Sortir");
		System.out.println("_______________________");
		
		do {
			System.out.print("Tria una opció: ");
			try {
				op=src.nextInt();
				if (op>=0 && op<=3) {
					correcte=true;
				}
			}
			catch (Exception e) {
				System.out.println("Error, opció no vàlida");
				src.nextLine();
				correcte=false;
			}
		} while (correcte==false);
		return op;
	}
}
