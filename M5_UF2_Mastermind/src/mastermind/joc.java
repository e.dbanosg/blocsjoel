package mastermind;

import java.util.Random;
import java.util.Scanner;

/**
 * Classe que conte els metodes necessaris per jugar una partida
 * @author David Banos Garcia
 * @version 30-01-2024
 */
public class joc {
	static Scanner src = new Scanner(System.in);
	
	/**
	 * A aquesta funcio es on inicialitzem les variables necessaries per poder jugar una partida i 
	 * truquem a les diferents funcions per poder jugar una partida correctament
	 * @param vector, vector que conte els numeros generats a endevinar
	 * @return 2, 1 o 0 en funcio del resultat final de la partida
	 */
	public static int jugarPartida(int [] vector) {
		boolean haGuanyat=false;
		boolean haPerdut=false;
		boolean inacabada=false;
		int [] vectornums;
		int intents=0;
		do {
			intents++;
			haPerdut=comprovarIntents(intents,vector);
			if (!haPerdut) {
				vectornums=numsJugador();
				haGuanyat=comprovarNums(vector,vectornums);
				if (!haGuanyat) {
					inacabada=terminarPartida();
				}
			}
		} while(haGuanyat==false && inacabada==false && haPerdut==false);
		
		if (haGuanyat) {
			System.out.println("Enhorabona!! Has guanyat la partida esbrinant la combinació en "+ intents + " intents.");
			return 1;
		} else if (inacabada) {
			return 2;
		} else {
			return 0;
		}
		
	}
	
	/**
	 * Funcio on cridem a la funció per demanar al jugador els numeros i 
	 * cridem a altra funció per comprovar que no hi hagi cap numero repetit
	 * @return vector amb numeros correctes i no repetits entrats per l'usuari
	 */
	private static int[] numsJugador() {
		boolean correcte = false;
		int [] vec;
		do {
			vec = obtenerNums();
			correcte = validar(vec);
		} while (!correcte);
		return vec;
	}
	
	/**
	 * Funcio on demanem al jugador els numeros que vol comparar amb el numeros generats per tal d'endevinar-los
	 * a aquesta funcio alhora de demanar cada numero truquem a la funcio demanarInt per controlar que la entrada es correcta
	 * @return vector amb els numeros entrats per l'usuari
	 */
	private static int[] obtenerNums() {
		// TODO Auto-generated method stub
		int cont=0;
		int [] vec=new int [4];
		do {
			int num= demanarInt();
			vec[cont]=num;
			cont++;
		} while(cont<4);
		System.out.println();
		return vec;
	}
	
	/**
	 * A aquesta funció controlem que no hi hagi cap numero repetit dels entrats per l'usuari
	 * @param vec, amb els numeros finals entrats per l'usuari
	 * @return false (algun numero repetit) o true(si no hi ha cap repetit)
	 */
	public static boolean validar(int[] vec) {
	    for (int i = 0; i < vec.length; i++) {
	        for (int j = i + 1; j < vec.length; j++) {
	            if (vec[i] == vec[j]) {
	                return false;
	            }
	        }
	    }
	    return true;
	}


	/**
	 * A aquesta funcio controlem que no es fessin mes de 15 intents
	 * @param intents, per controlar el numero d'intents
	 * @param vector, per informar a l'usuari de la combinacio de numeros que ha intentat endevinar
	 * @return true o false, si retornem true l'usuari a perdut i es finalitza la partida, sino continuem amb la partida
	 */
	 private static boolean comprovarIntents(int intents, int[] vector) {
		// TODO Auto-generated method stub
		if (intents>15) {
			System.out.println("Ooohhh! Has perdut. La combinació que buscaves és: " +vector[0]+" "+vector[1]+" "+vector[2]+" "+vector[3]);
			return true;
		} else {return false;}
	}

	/**
	 * A aquesta funcio es on donem a l'usuari l'opcio de continuar amb la partida o no, 
	 * tambe controlem que la entrada de l'usuari sigui la esperada 
	 * @return true o false indicant si vol continuar jugant o no
	 */
	private static boolean terminarPartida() {
		// TODO Auto-generated method stubç
		boolean correcte=false;
		int num=0;
		System.out.println("Vols continuar jugant?");
		System.out.println("0.- Si");
		System.out.println("1.- No");
		
		do {
			try {
				num=src.nextInt();
				if (num==1 || num==0) {
					correcte=true;
				} else {System.out.println("Has d'introduir un 0 o un 1");}
			} 
			catch(Exception e) {
				System.out.println("La entrada es incorrecta");
				src.nextLine();
			}
		} while (correcte==false);
		
		if (num==1) {
			System.out.println("Ops! Has deixat la partida sense acabar.");
			return true;
		} else {
			System.out.println("Continuem amb la partida");
			return false;
		}
	}

	/**
	 * A aquesta funcio comprovem si ha encertat per cada numero entrat per l'usuari si ha encertat o no i si ha fet ple o no
	 * @param vector, vector amb els numeros generats
	 * @param vectornums, vector amb els numeros escrits per l'usuari
	 * @return false o true, per indicar si l'usuari a guanyat o no
	 */
	public static boolean comprovarNums(int[] vector, int[] vectornums) {
		// TODO Auto-generated method stub
		int contPle=0;
		int contEncer=0;
		for (int i=0;i<vector.length;i++) {
			if (vectornums[0]==vector[i]) {
				if (i==0) {
					contPle++;
				} else {
					contEncer++;
				}
			}
			if (vectornums[1]==vector[i]) {
				if (i==1) {
					contPle++;
				} else {
					contEncer++;
				}
			}
			if (vectornums[2]==vector[i]) {
				if (i==2) {
					contPle++;
				} else {
					contEncer++;
				}
			}
			if (vectornums[3]==vector[i]) {
				if (i==3) {
					contPle++;
				} else {
					contEncer++;
				}
			}
			
		}
		if (contPle>0 && contEncer>0) {
			System.out.println(contPle+" ple i "+contEncer+" encertat");
		} else if (contPle>0) {
			System.out.println(contPle+" ple");
		} else if (contEncer>0) {
			System.out.println(contEncer+" encertat");
		} else {
			System.out.println("Cap numero encertat");
		}
		
		if (contPle+contEncer==4) {
			return true;
		} else {return false;}
		
	}
	
	/**
	 * A aquesta funcio controlem que qualsevol numero que demanem a l'usuari per a que tracti 
	 * d'endevinar els numeros generats sigui correcte, 
	 * @return el numero entrar per l'usuari una vegada es correcte
	 */
	private static int demanarInt() {
		// TODO Auto-generated method stub
		boolean correcte=false;
		int num=0;
		do {
			try {
					System.out.print("Introdueix un numero entre del 0 al 9 inclosos: ");
					num=src.nextInt();
					if (num<0 || num>9) {
						System.out.println("El numero ha d'estar entre el 0 i el 9");
						correcte=false;
					} else {
						correcte=true;
					}
				}
				catch(Exception e) {
					System.out.println("La entrada es incorrecta");
					src.nextLine();
					correcte=false;
				}
		} while (correcte==false);
		return num;
	}

	/**
	 * Generem els 4 numeros a endevinar sense repeticions
	 * @return vector amb els numeros a endevinar generats
	 */
	public static int[] generarNums() {
		// TODO Auto-generated method stub
		int []vector= new int [4];
		
		int [] nums= {0,1,2,3,4,5,6,7,8,9};
		
		Random random = new Random();
		int cont=0;
		int[] contCar = new int[nums.length];
		int a;
		
		do {
			do {a = random.nextInt(10);
			} while (contCar[a]>=1);
			vector[cont]=a;
			contCar[a]++;
			cont++;
		} while(cont<4);
		return vector;
	}
	

}
