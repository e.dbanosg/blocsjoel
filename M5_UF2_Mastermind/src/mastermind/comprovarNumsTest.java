package mastermind;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class comprovarNumsTest {
	private int [] vecUser;
	private int [] vecOcul;
	private boolean resul;
	
	private static int [][] vecsUser= {
			{6728,647890,367189,61789444}, //true
			{100,1000,10000,100000}, //true
			{7,23,4,32}, //true
			{-32,-64,-1,-2}, //false
			{1,2,3,4}, //true
			{-456278,-53267382,-5676879,-0}, //true
			{-100,-1000,-10000,-100000}, //true
			{-1,-2,-3,-9}, //false
			{-567890,-1987675,-987656,-4526789}, //false
			{567289890,567687932,3,2}, //false
			{-1,-2,-9,-8}, //false
			{6783,4254,1,5} //true
			};
	
	private static int [][] vecsOcul= {
			{6728,647890,367189,61789444},
			{100,1000,10000,100000},
			{7,23,4,32},
			{-647,-3412,-43,-321},
			{1,2,3,4},
			{-456278,-53267382,-5676879,-0},
			{-100,-1000,-10000,-100000},
			{-1,-2,-3,-4},
			{567890,1987675,987656,4526789},
			{-567289890,-567687932,-3,-2},
			{-10,-2,-9,-8},
			{6783,4254,1,5}
			};
	
	public comprovarNumsTest(int [] vecUser,int []vecOcul, boolean resul) {
		this.vecUser=vecUser;
		this.vecOcul = vecOcul;
		this.resul=resul;
	}
	
	@Parameters
	public static Collection<Object[]> numeros() {
		return Arrays.asList(new Object[][] {
			{vecsUser[0],vecsOcul[0], true},
			{vecsUser[1],vecsOcul[1], true},
			{vecsUser[2],vecsOcul[2], true},
			{vecsUser[3],vecsOcul[3], false},
			{vecsUser[4],vecsOcul[4], true},
			{vecsUser[5],vecsOcul[5], true},
			{vecsUser[6],vecsOcul[6], true},
			{vecsUser[7],vecsOcul[7], false},
			{vecsUser[8],vecsOcul[8], false},
			{vecsUser[9],vecsOcul[9], false},
			{vecsUser[10],vecsOcul[10], false},
			{vecsUser[11],vecsOcul[11], true}
		});
		
	}
	@Test
	public void testValidar() {
		boolean res = joc.comprovarNums(vecUser,vecOcul);
		assertEquals(resul,  res);
	}

}
