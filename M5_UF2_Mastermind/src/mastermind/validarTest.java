package mastermind;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class validarTest {

	private int [] vec;
	private boolean resul;
	
	private static int [][] vecs= {
							{1,2,3,4}, //true
							{100,1000,10000,100000}, //true
							{832,9328,1832,832}, //false
							{9898,55467890,9898,0}, //false
							{-12,-542,-5372,-516}, //true
							{-40,-31,-4,-31}, //false
							{-100,-1000,-10000,-100000}, //true
							{-1,-2,-3,-4}, //true
							{-92100,-921,-9210,-921}, //false
							{-5,-5,-5,-5}, //false
							{9,9,9,9}, //false
							{0,531678,523678198,517687} //true
							};
	
	public validarTest(int [] vec, boolean resul) {
		this.vec = vec;
		this.resul=resul;
	}
	
	@Parameters
	public static Collection<Object[]> numeros() {
		return Arrays.asList(new Object[][] {
			{vecs[0], true},
			{vecs[1], true},
			{vecs[2], false},
			{vecs[3], false},
			{vecs[4], true},
			{vecs[5], false},
			{vecs[6], true},
			{vecs[7], true},
			{vecs[8], false},
			{vecs[9], false},
			{vecs[10], false},
			{vecs[11], true}
		});
		
	}
	@Test
	public void testValidar() {
		boolean res = joc.validar(vec);
		assertEquals(resul,  res);
	}

}
