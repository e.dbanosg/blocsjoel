package inicials;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Unique {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src= new Scanner (System.in);
	
		int casos = src.nextInt();
		
        while (casos > 0) {
            int K = src.nextInt();
            ArrayList<String> lista= new ArrayList<String>();
            src.nextLine();
            
            int i = 0;
            while (i < K) {
                String nom = src.next();
                lista.add(nom);
                i++;
            }
            Set listaclean = new HashSet<>(lista);
            lista.clear();
            lista.addAll(listaclean);
            
            System.out.println(lista);
            casos--;
	}

}
}
