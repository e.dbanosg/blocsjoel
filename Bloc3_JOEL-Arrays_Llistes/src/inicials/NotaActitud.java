package inicials;

import java.util.Scanner;

public class NotaActitud {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src= new Scanner(System.in);
		int casos=src.nextInt();
		int i;
		while(casos>0) {
			i=0;
			
			int K=src.nextInt();
			int [] vectorNo= new int [K];
			int [] vectorSi= new int [K];
			
			while (i<K) {
				vectorNo[i]=src.nextInt();
				i++;
			}
			
			int j=0;
			while (j<K) {
				vectorSi[j]=src.nextInt();
				j++;
			}
			
			int max=src.nextInt();
			int nodispar=0; 
			
			for (int cont=0;cont<K;cont++) {
				if (vectorNo[cont]+max==vectorSi[cont]) {
					nodispar++;
				}
			}
			System.out.println(nodispar);
			casos--;
		}
	}

}
