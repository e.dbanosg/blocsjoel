package inicials;

import java.util.Scanner;

public class capicua {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
        String N = src.nextLine();

        String reversed = new StringBuilder(N).reverse().toString(); 
        //se crea un objeto StringBuilder con la cadena N, 
        //se llama al método reverse() para invertir orden caracteres
        //convertir contenido StringBuilder a String

        if (N.equals(reversed)) {
            System.out.println("SI"); // El número es capicúa
        } else {
            System.out.println("NO"); // El número no es capicúa
        }
		
		
		/*
		Scanner src = new Scanner(System.in);
		long inverso=0;
		
		long cifras;
		
		long N=src.nextLong();
		
		long aux=N;
		
		while (aux>0) {
			cifras= aux % 10; //vamos obteniendo el ultimo numero del numero entrado
			inverso = inverso * 10 + cifras; //guardamos el utlimo numero i dejamos hueco para el siguiente
			aux = aux / 10; //borramos ultimo numero del numero entrado
			//el resultado de inverso es el numero entrado pero al reves
		}
		
		if (N==inverso) {
			System.out.println("SI"); //el numero es capicua
		} else {System.out.println("NO");} //el numero no es capicua
		 */
	}

}
