
package inicials;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;

public class FrancescoVirgolini {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);

		int casos = src.nextInt();
		src.nextLine();

		for (int c = 0; c < casos; c++) {
			int K = src.nextInt();
			src.nextLine();
			ArrayList<String> lista = new ArrayList<String>();
			int i = 0;

			while (i < K) {
				String nom = src.nextLine();
				lista.add(nom);
				i++;
			}

			for (int cont = 0; cont < K; cont++) {
				if (lista.get(cont).equals("Francesco Virgolini")) {
					Collections.swap(lista, lista.indexOf("Francesco Virgolini"),
							lista.indexOf("Francesco Virgolini") - 1);
				}
			}

			System.out.println(lista);

		}

	}
}