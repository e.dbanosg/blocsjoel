package inicials;

import java.util.ArrayList;
import java.util.Scanner;

public class Repetits {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);

		int casos = src.nextInt();
		src.nextLine();

		for (int c = 0; c < casos; c++) {
			int K = src.nextInt();
			src.nextLine();
			ArrayList<Integer> lista = new ArrayList<Integer>();
			int i = 0;

			while (i < K) {
				int num = src.nextInt();
				lista.add(num);
				i++;
			}
			
			boolean repetit=false;
			
			int j=0;
			while (repetit==false && j<lista.size()) {
				int temp= lista.get(j);
				if (lista.contains(temp)) {
					lista.remove(lista.get(j));
					if (lista.contains(temp)) {
						repetit=true;
					}
				}
				j++;
			}
			
			if (repetit) {
				System.out.println("SI");
			} else {System.out.println("NO");}
			
		}
	}

}
