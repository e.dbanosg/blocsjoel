package inicials;

import java.util.Scanner;

public class BloodborneArrays {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src= new Scanner (System.in);
		
		int casos=src.nextInt();
		
		for (;casos>0;casos--) {
			int K= src.nextInt();
			int [] vector= new int [K];
			
			int i=0;
			
			while (i<K) {
				vector[i]=src.nextInt();
				i++;
			}
			
			boolean seguit= false;
			
			for (int j=0;j<K-1;j++) {
				if (vector[j]==vector[j+1]) {
					seguit=true;
				}
			}
			if (seguit) {
				System.out.println("SI");
			} else {System.out.println("NO");}
		}
	}

}
