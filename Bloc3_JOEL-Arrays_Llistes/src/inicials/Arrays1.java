package inicials;

import java.util.Scanner;

public class Arrays1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src= new Scanner(System.in);
		
		final int K=src.nextInt();    //constant que indica la grandaria del vector
		int [] vector= new int [K];   //creem vector
		
		int i=0;
		while (i<K) {
			vector[i]=src.nextInt();
			i++;
		}
		//ara el valor de i es K
		
		
		//legim la posició del vector que volem mostrar
		int pos= src.nextInt();
		
		//mostrem contingut vector
		i=0;
		while (i<K) {
			System.out.print(vector[i] + " ");   
			i++;
		}
		
		//mostrem contingut de la posició pos
		System.out.println("\n" + vector[pos]);
		
		
	}

}
