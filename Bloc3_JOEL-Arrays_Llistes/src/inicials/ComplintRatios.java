package inicials;

import java.util.ArrayList;
import java.util.Scanner;

public class ComplintRatios {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		int casos = src.nextInt();
		
        while (casos > 0) {
            int K = src.nextInt();
            ArrayList<String> lista= new ArrayList<String>();
            src.nextLine();
            
            int i = 0;
            while (i < K) {
                String nom = src.next();
                lista.add(nom);
                i++;
            }

            int pozo = src.nextInt();
            lista.remove(pozo);
            for (String nombre : lista) {
                System.out.print(nombre + " ");
            }
            casos--;
	}

}
}