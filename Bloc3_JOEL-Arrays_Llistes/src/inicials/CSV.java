package inicials;

import java.util.Scanner;

public class CSV {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);

		String entrada = src.nextLine();

		String[] parts = entrada.split(";");

		for (int i = 0; i < parts.length; i++) {
			String part = parts[i];
			if (part.length() >= 10) {
				part = "ERROR";
			}
			if (i + 1 == parts.length) {
				System.out.println(part);
			} else
				System.out.print(part + ";");
		}

	}

}
